USE [master]
GO
/****** Object:  Database [WeeJoby]    Script Date: 21/01/2016 19:40:18 ******/
CREATE DATABASE [WeeJoby]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'WeeJoby', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\WeeJoby.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'WeeJoby_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\WeeJoby_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [WeeJoby] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [WeeJoby].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [WeeJoby] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [WeeJoby] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [WeeJoby] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [WeeJoby] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [WeeJoby] SET ARITHABORT OFF 
GO
ALTER DATABASE [WeeJoby] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [WeeJoby] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [WeeJoby] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [WeeJoby] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [WeeJoby] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [WeeJoby] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [WeeJoby] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [WeeJoby] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [WeeJoby] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [WeeJoby] SET  DISABLE_BROKER 
GO
ALTER DATABASE [WeeJoby] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [WeeJoby] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [WeeJoby] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [WeeJoby] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [WeeJoby] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [WeeJoby] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [WeeJoby] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [WeeJoby] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [WeeJoby] SET  MULTI_USER 
GO
ALTER DATABASE [WeeJoby] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [WeeJoby] SET DB_CHAINING OFF 
GO
ALTER DATABASE [WeeJoby] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [WeeJoby] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [WeeJoby] SET DELAYED_DURABILITY = DISABLED 
GO
USE [WeeJoby]
GO
/****** Object:  Table [dbo].[News]    Script Date: 21/01/2016 19:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](150) NOT NULL,
	[content] [text] NULL,
	[slug] [varchar](150) NOT NULL,
	[datePublished] [datetime] NULL,
	[dateCreated] [datetime] NOT NULL,
	[authorId] [int] NOT NULL,
	[featureImage] [varchar](150) NULL,
 CONSTRAINT [PK_News_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Posts]    Script Date: 21/01/2016 19:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Posts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[content] [text] NOT NULL,
	[dateCreated] [datetime] NOT NULL CONSTRAINT [DF_Posts_DateCreated]  DEFAULT (getdate()),
	[datePublished] [datetime] NULL,
	[authorId] [int] NOT NULL,
	[slug] [nvarchar](150) NOT NULL,
	[type] [nchar](150) NULL,
 CONSTRAINT [PK_Posts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[News] ON 

INSERT [dbo].[News] ([id], [title], [content], [slug], [datePublished], [dateCreated], [authorId], [featureImage]) VALUES (4, N'Some New News!', N'Some New News!', N'some-new-news', CAST(N'2015-01-20 00:00:00.000' AS DateTime), CAST(N'2015-01-20 00:00:00.000' AS DateTime), 1, N'images/news-item-2.gif')
INSERT [dbo].[News] ([id], [title], [content], [slug], [datePublished], [dateCreated], [authorId], [featureImage]) VALUES (5, N'New New ;) News', N'oooo freakout', N'pellets', CAST(N'2016-01-20 00:00:00.000' AS DateTime), CAST(N'2016-01-20 00:00:00.000' AS DateTime), 1, NULL)
INSERT [dbo].[News] ([id], [title], [content], [slug], [datePublished], [dateCreated], [authorId], [featureImage]) VALUES (6, N'Newer New ;)  News', N'wow', N'slug-number-1', CAST(N'2015-01-20 00:00:00.000' AS DateTime), CAST(N'2015-01-20 00:00:00.000' AS DateTime), 1, N'images/news-item-3.gif')
SET IDENTITY_INSERT [dbo].[News] OFF
SET IDENTITY_INSERT [dbo].[Posts] ON 

INSERT [dbo].[Posts] ([id], [Title], [content], [dateCreated], [datePublished], [authorId], [slug], [type]) VALUES (6, N'The *UK''s No.1* private lable household paper manufacturer', N'Lorem ipsum dolor sit amet, consectet uer adipiscing eli sed nostrud exerci tation ex ea commodo con uis autem vel eum iriure dolor ln hendre rit in vulpu.', CAST(N'2016-01-19 21:08:21.623' AS DateTime), NULL, 1, N'uks-number-one-paper-manafacturer', NULL)
INSERT [dbo].[Posts] ([id], [Title], [content], [dateCreated], [datePublished], [authorId], [slug], [type]) VALUES (7, N'LPC Environment', N'Lorem ipsum dolor doloripsum dolor consectet rem ipsu m dolor consec.', CAST(N'2016-01-19 21:42:15.640' AS DateTime), NULL, 1, N'lpc-environment', NULL)
INSERT [dbo].[Posts] ([id], [Title], [content], [dateCreated], [datePublished], [authorId], [slug], [type]) VALUES (8, N'Welcome to LPC Group', N'Lorem ipsum dolor sit amet, conse ctet uer adipiscing elit,sed nostrud exerci tation ex ea commodo consequat. 

Duis autem vel eum iriLlrc clolur in henclre rit in vulputtate velit esse mo lestie hendr erit in vulputate velit praesent Luptatum zzril elenit augue dlis dolore te.', CAST(N'2016-01-19 21:50:08.657' AS DateTime), NULL, 1, N'welcome-to-lpc-group', NULL)
INSERT [dbo].[Posts] ([id], [Title], [content], [dateCreated], [datePublished], [authorId], [slug], [type]) VALUES (9, N'Product lnnovation', N'
Lorem ipsum dolor sit amet, consectet uer adipiscing elit, sed nostrud exerci tation ex ea commodo consequat.', CAST(N'2016-01-19 21:58:48.700' AS DateTime), NULL, 1, N'product-lnnovation', NULL)
INSERT [dbo].[Posts] ([id], [Title], [content], [dateCreated], [datePublished], [authorId], [slug], [type]) VALUES (10, N'LPC Group', N'



Lorem ipsum dolor sit amet, consectet uer adipiscing elit, sed nostrud exerci tation ex ea commodo consequat.', CAST(N'2016-01-19 22:05:45.143' AS DateTime), NULL, 1, N'lpc-group', NULL)
INSERT [dbo].[Posts] ([id], [Title], [content], [dateCreated], [datePublished], [authorId], [slug], [type]) VALUES (11, N'LPC Latest News', N'LPC Latest News', CAST(N'2016-01-19 22:36:58.927' AS DateTime), NULL, 1, N'lpc-latest-news', NULL)
SET IDENTITY_INSERT [dbo].[Posts] OFF
USE [master]
GO
ALTER DATABASE [WeeJoby] SET  READ_WRITE 
GO
