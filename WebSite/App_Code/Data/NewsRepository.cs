﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Summary description for NewsRepository
/// </summary>
public class NewsRepository
{
    private static readonly string _connectionString = "DefultConnection";

    public NewsRepository()
    { 
            
    }

    public static dynamic Get(int id)
    {
        using (var db = Database.Open(_connectionString))
        {
            var sql = "SELECT * FROM News WHERE id = @0";

            return db.QuerySingle(sql, id);
        }
    }

    public static dynamic Get(string slug)
    {
        using (var db = Database.Open(_connectionString))
        {
            var sql = "SELECT * FROM News WHERE slug = @0";

            return db.QuerySingle(sql, slug);
        }
    }

    public static IEnumerable<dynamic> GetAll(string orderBy = null)
    {
        using (var db = Database.Open(_connectionString))
        {
            var sql = "SELECT * FROM News";

            if (!string.IsNullOrEmpty(orderBy))
            {
                sql += " ORDER BY " + orderBy;
            }

            return db.Query(sql);
        }
    }

    public static void Add(string title, string content, string slug, DateTime? datePublished, int authorId, string image)
    {
        using (var db = Database.Open(_connectionString))
        {
            var sql = "INSERT INTO News (title, content, datePublished, authorId, slug, featureImage) " +
                "VALUES (@0, @1, @2, @3, @4, @5)";

            db.Execute(sql, title, content, datePublished, authorId, slug, image);
        }
    }

    public static void Edit(int id, string title, string content, string slug, DateTime? datePublished, string image, int authorId)
    {
        using (var db = Database.Open(_connectionString))
        {
            var sql = "UPDATE News SET title = @0, content = @1, " +
                "datePublished= @2, authorId = @3, slug = @4, featureImage = @5 " +
                    "WHERE id = @6";

            db.Execute(sql, title, content, datePublished, authorId, slug, image,  id);
        }
    }

    public static void Remove(string slug)
    {
        using (var db = Database.Open(_connectionString))
        {
            var sql = "DELETE FROM News WHERE slug = @0";

            db.Execute(sql, slug);
        }
    }
}