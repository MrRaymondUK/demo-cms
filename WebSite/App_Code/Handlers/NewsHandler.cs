﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using WebMatrix.Data;
using Microsoft.Ajax.Utilities;

/// <summary>
/// Summary description for PostHandlers
/// </summary>
public class NewsHandler : IHttpHandler
{
    public NewsHandler()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public bool IsReusable
    {
        get { return false; }
    }

    public void ProcessRequest(HttpContext context)
    {
        var mode = context.Request.Form["mode"];
        var title = context.Request.Form["newsTitle"];
        var content = context.Request.Form["newsContent"];
        var slug = context.Request.Form["newsSlug"];
        var image = context.Request.Form["newsFeatureImage"];
        var id = context.Request.Form["newsId"];
        var datePublished = context.Request.Form["newsDatePublished"];

        if (string.IsNullOrWhiteSpace(slug))
        {
            slug = CreateSlug(title);
        }

        if (mode == "edit")
        {
            Edit(Convert.ToInt32(id), title, content, slug, datePublished, 1, image);
            context.Response.Redirect("~/admin/news/");
        }
        else if (mode == "new")
        {
            Create(title, content, slug, datePublished, 1, image);
            context.Response.Redirect("~/admin/news/");
        }
        else if (mode == "delete")
        {
            Delete(slug);
            context.Response.Redirect("~/admin/news/");
        }
    }

    private static void Create(string title, string content, string slug, string datePublished, int authorId, string image)
    {
        var result = NewsRepository.Get(slug);
        DateTime? published = null;

        if (result != null)
        {
            throw new HttpException(409, "Slug is already in use.");
        }

        if (!string.IsNullOrWhiteSpace(datePublished))
        {
            published = DateTime.Parse(datePublished);
        }

        NewsRepository.Add(title, content, slug, published, authorId, image);
    }

    private static void Edit(int id, string title, string content, string slug, string datePublished, int authorId, string image)
    {
        var result = NewsRepository.Get(id);
        DateTime? published = null;

        if (result == null)
        {
            throw new HttpException(404, "Post dose not exist.");
        }

        if (!string.IsNullOrWhiteSpace(datePublished))
        {
            published = DateTime.Parse(datePublished);
        }
        // NewsRepository.Add(title, content, slug, null, authorId);
        NewsRepository.Edit(id, title, content, slug, published, image, authorId);
    }


    private static void Delete(string slug)
    {
        NewsRepository.Remove(slug);
    }

    private static string CreateSlug(string title)
    {
        title = title.ToLowerInvariant().Replace(" ", "-");
        title = Regex.Replace(title, @"[^0-9a-z-]", string.Empty);

        return title;
    }

}