﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.WebPages;
using WebMatrix.Data;

/// <summary>
/// Summary description for News
/// </summary>
public class News
{
    public News()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private static WebPageRenderingBase page
    {
        get { return WebPageContext.Current.Page;  }
    }

    public static string mode
    {

        get
        {
            if (page.UrlData.Any())
            {
                return page.UrlData[0];
            }
            return string.Empty;
        }
    }

    public static string slug
    {
        get
        {
            if (mode != "new")
            {

                return page.UrlData[1];
            }

            return string.Empty;
        }
    }

    public static dynamic Current
    {
        get
        {         
           var reust = NewsRepository.Get(slug);
  
           return reust ?? CraeteNewsObject();
        }
    }

    private static dynamic CraeteNewsObject()
    {
        dynamic obj = new ExpandoObject();

        obj.Id = 0;
        obj.Title = string.Empty;
        obj.Content = string.Empty;
        obj.DateCreated = DateTime.Now;
        obj.DatePublished = null;
        obj.Slug = string.Empty;
        obj.AuthorId = null;
        obj.FeatureImage = string.Empty;

        return obj;
    }

}