﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.WebPages;
using WebMatrix.Data;

/// <summary>
/// Summary description for Post
/// </summary>
public class Post
{
    public Post()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private static WebPageRenderingBase page
    {
        get { return WebPageContext.Current.Page;  }
    }

    public static string mode
    {

        get
        {
            if (page.UrlData.Any())
            {
                return page.UrlData[0];
            }
            return string.Empty;
        }
    }

    public static string slug
    {
        get
        {
            if (mode != "new")
            {

                return page.UrlData[1];
            }

            return string.Empty;
        }
    }

    public static dynamic Current
    {
        get
        {         
           var reust = PostRepository.Get(slug);
  
           return reust ?? CraetePostObject();
        }
    }

    private static dynamic CraetePostObject()
    {
        dynamic obj = new ExpandoObject();

        obj.Id = 0;
        obj.Title = string.Empty;
        obj.Content = string.Empty;
        obj.DateCreated = DateTime.Now;
        obj.DatePublished = null;
        obj.Slug = string.Empty;
        obj.AuthorId = null;

        return obj;
    }

}