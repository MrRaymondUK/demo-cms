#Demo CMS for Verto#

*Author: Raymond Marriott-Smith*
*Version: 1.0.0*

##About##
This website is my first ASP.NET and C# project. This has been an enjoyable challenge and I have learned a lot (*mainly from YouTube and Stack Overflow*). 

Surprisingly one of the main challenges I have faced was getting to grips with the Visual Studios IDE which was quite different from other IDEs and text editors I have used in the past. However I have come to really like visual studios and the power it has when it comes to building projects.

If I had more time and flexibility I would have done the following:

* Made the front end website responsive.
* Built an Admin dashboard (with fancy graphs and stuff).
* Added authentication and user management.
* Figured out how make content editing easier.


##Installation Instructions##

To install the demo CMS you will need to do the following:

1. Download the code in this repository.
2. Open the file named `script.sql` in Microsoft SQL Server Management Studio then Execute the script.
3. Open Microsoft Visual Studio then go to `File > Open > Web Site` and select the `WebSite` folder. The contents of the folder should now show up in solution explorer. 
5. To run the website in your browser by going to `Debug > Start Without Debugging`.

##Using the CMS##
To access the CMS you will need to go to your `localhost` with the port number Microsoft Visual Studios is using, followed by `/admin/posts` for example:  `http://localhost:50951/admin/post`
